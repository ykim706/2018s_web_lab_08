'use strict';
$(document).ready(function () {
    for (let i = 1; i < 7; i++) {
        $("#radio-background" + i).change(function () {
            let ext = '.jpg';
            if (i == 6) {
                ext = '.gif';
            }
            $("#background").attr('src', '../images/background' + i + ext);
        })
    }



    for (let i = 1; i < 9; i++) {
        $("#check-dolphin" + i).change(function () {
            if($(this).is(':checked')){
                $('#dolphin' + i).show();
            }
            else{
                $('#dolphin' + i).hide();
            }

        }).change();
    }


    $("#vertical-control").on('input', function(){$(".dolphin").css("transform", 'translateY('+$(this).val() + 'px)' );});

    $("#size-control").on('input', function(){$(".dolphin").css("transform",'scale(' + (1 + (+$(this).val() / 100)) + ')');
    });
    $("#horizontal-control").on('input', function(){$(".dolphin").css("transform", 'translateX('+$(this).val() + 'px)')});

    // TODO: Your code here
});
